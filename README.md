**Synthisesing data for WMC Hackathon**

For this project I was given the task of creating a linked dataset from the 
NHS Wales Informatics service Data warehouse and creating a synthesised dataset
so that non NHS Staff were able to work with it.

There are many tools available when synthesising data that fall on the following
continuum. 

![alt text](https://gitlab.com/data-science-projects-Arthur/datasynth/uploads/be345ebf6d50e3b596aed5fc0a65feb6/Slide1.PNG)


Where disclosure means the risk of an actual patient record being disclosed.
In general most products are a tradeoff somewhere in the middle.
For this project we were very conscious of disclosure risk so we erred more towards
the right side of the spectrum.

After considering several tools we selected the R package synthpop. 
We did so for several reasons:
1. It is quick to synthesise(we had a limited time constraint of 2 weeks)
2. It has a much lower disclosure risk than Neural Network approaches like GAN's
3. It is simpler to explain you have done due dilligence to management.

To further limit the risk of disclosure we made sure that the source dataset was
sufficiently coarse to minimize the risk of disclosure(aggregating low numbers).

In addition to the R notebook in this repo outlining the synthesis there is also
exploratory work completed in python using pandas, pandas-profiling etc to work out 
the relationships between the variables in the source dataset and ensure they follow
the same trends in the synthetic data.

This is not included in the public repo since it contains actual patient records 
and I wish to limit the risk of disclosure. 

The primary methadology for determining relationships were:
1. Correlation Matrices
2. Feature Importances(ExtraTrees)

These are then added to the prediction matrix in the notebook






